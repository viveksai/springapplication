package com.example.validation;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WavelabsRepository  extends JpaRepository<WavelabsEmployee, String> {

	List<WavelabsEmployee> findAll();
	
}
