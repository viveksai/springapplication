package com.example.onetoone.programfinal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnetponeprogramfinalApplication  implements CommandLineRunner{
	@Autowired
    private UserRepository users;

    @Autowired
    private FreePageRepository freePages;

    public static void main(String[] args) {
        SpringApplication.run(OnetponeprogramfinalApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        // Clean all data from tables
        this.freePages.deleteAllInBatch();
        this.users.deleteAllInBatch();

        // Create two users
        User user1 = new User("anvesh", "Smith", "anveshsmith@email.com", "anvesh123");
        User user2 = new User("Lia", "Smith", "liasmith@email.com", "lia123");

        // Create two free pages
        FreePage freePage1 = new FreePage("anvesh-project", "https://www.anveshsite.com");
        FreePage freePage2 = new FreePage("lia-project", "https://www.lisite.com");

        // Set the free page to the respective user
        user1.setFreePage(freePage1);
        user2.setFreePage(freePage2);

        // Set the user for the respective page
        freePage1.setUser(user1);
        freePage2.setUser(user2);

        // Persist data to database
        this.users.save(user1);
        this.users.save(user2);

    }
}
