package com.example.onetoone.programfinal;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FreePageRepository extends JpaRepository<FreePage, Long>{

}
