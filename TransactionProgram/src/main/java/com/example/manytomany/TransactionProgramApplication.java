package com.example.manytomany;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
@SpringBootApplication
@EnableTransactionManagement
@EnableJpaRepositories
@EnableCaching

public class TransactionProgramApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionProgramApplication.class, args);
	}

}
