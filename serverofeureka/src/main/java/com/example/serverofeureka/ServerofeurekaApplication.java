package com.example.serverofeureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
@EnableEurekaServer
@SpringBootApplication
public class ServerofeurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerofeurekaApplication.class, args);
	}

}
