package com.example.eurekaproject2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
@EnableCaching
@SpringBootApplication
public class CatcheprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatcheprojectApplication.class, args);
	}

}
