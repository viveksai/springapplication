package com.example.eurekaproject2;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
@Service
public class EmployeeCacheService {
	
	@Autowired
	public EmployeeCacheRepository emprepository;
	
	@org.springframework.cache.annotation.Cacheable(cacheNames = "person")
	public List<EmployeeCache> getAllEmployeeCache() 
	{
		List<EmployeeCache> li=emprepository.findAll();
		return li;
		
	}
	public EmployeeCache saveEmployeeCache(EmployeeCache cacheee) {
		return emprepository.save(cacheee);
	}

	private void printTransactionInfo() {
		if (TransactionAspectSupport.currentTransactionStatus().isNewTransaction()) {
			System.out.println("new transaction");
		} else {
			System.out.println("old transaction");
		}
	}
	@org.springframework.cache.annotation.Cacheable(cacheNames = "students")
	public EmployeeCache getEmployeeCache(Integer id) {
		return emprepository.findById(id).get();
	}

	@CachePut(cacheNames = "students", key = "#po.empid")
	public EmployeeCache updateEmployeeCache(EmployeeCache cacheee, Integer id) {
		cacheee.setEmpid(id);
		EmployeeCache student2 = emprepository.saveAndFlush(cacheee);
		return student2;
	}

	@CacheEvict(key = "#id", cacheNames = "students")
	public void deleteEmployeeCache(Integer id) {
		emprepository.deleteById(id);
	}
	

}

