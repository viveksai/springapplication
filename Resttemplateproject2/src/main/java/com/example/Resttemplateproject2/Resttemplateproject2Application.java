package com.example.Resttemplateproject2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Resttemplateproject2Application {

	public static void main(String[] args) {
		SpringApplication.run(Resttemplateproject2Application.class, args);
	}

}
