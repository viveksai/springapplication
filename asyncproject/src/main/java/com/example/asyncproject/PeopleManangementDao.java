package com.example.asyncproject;

import java.util.concurrent.CompletableFuture;

import org.springframework.data.repository.CrudRepository;
import org.springframework.scheduling.annotation.Async;

public interface PeopleManangementDao extends CrudRepository<Person, Integer>{

	CompletableFuture<Person> findByEmail(String email);
	
}


